<?php
/*
 * Creacion de variables
 */
$nombre="Ramon"; // variable de tipo string
$apellidos="Abramo Feijoo";
$edad=50; // variable tipo entero

// Reto
// a partir de aqui con las variables quiero que
// mostreis esto
// <div>Nombre: Ramon</div>
// <div>Apellidos: Abramo Feijoo</div>
// <div>Edad: 50</div>

?>
<h2>Opcion 1</h2>
<div>Nombre: <?= $nombre; ?> </div>
<div>Apellidos: <?= $apellidos; ?> </div>
<div>Edad: <?= $edad; ?> </div>

<h2>Opcion 2</h2>
<div>Nombre: <?php echo $nombre; ?> </div>
<div>Apellidos: <?php echo $apellidos; ?> </div>
<div>Edad: <?php $edad; ?> </div>

<h2>Opcion 3</h2>
<?php
    echo "<div>Nombre: {$nombre}</div>";
    echo "<div>Apellidos: {$apellidos} </div>";
    echo "<div>Edad: {$edad}</div>";
?>



        
