<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        /* 
         * creando una variable
         */
                
        // array enumerado
        $vector=[10,"suspenso",3,8,5,6];
        
        // metodo obsoleto
        // $vector=array(10,"suspenso",2,3,5,6);
        
        var_dump($vector); // depurando la variable vector
        
        // array asociativo
        $alumno=[
            "nombre" => "Josefa",
            "edad" => 65
        ];
        
        var_dump($alumno); // depurando la variable alumno
        
        ?>
    </body>
</html>
