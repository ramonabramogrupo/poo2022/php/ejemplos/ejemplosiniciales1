<?php

// listado de los numeros 90 al 80
// utilizar listas no numerados

?>

<ul>
<?php
// principio del bucle
    for($contador=90;$contador>=80;$contador--){
        //echo "<li>{$contador}</li>";
?>
    <li><?= $contador ?></li>
<?php
    }
// final del bucle
?>
</ul>



<?php
// listado del 1 al 50 pero 
// solo los numeros impares
// con lista no numerada

echo "<ul>";
for($contador=1;$contador<50;$contador=$contador+2){
    echo "<li>{$contador}</li>";
}
echo "</ul>";
